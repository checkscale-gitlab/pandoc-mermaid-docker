# Another Pandoc Mermaid Docker Image

Try to build a Docker image online.

This image uses [Ubuntu Mono][ubuntu-fonts] and [Noto Emoji][noto-emoji] fonts
respectively license under [Ubuntu Font License][ubu-lic] and
[SIL Open Font License v1.1][noto-lic].

[ubuntu-fonts]: https://design.ubuntu.com/font/
[noto-emoji]: https://github.com/googlefonts/noto-emoji
[ubu-lic]: https://ubuntu.com/legal/font-licence
[noto-lic]: https://github.com/googlefonts/noto-emoji/blob/master/fonts/LICENSE
